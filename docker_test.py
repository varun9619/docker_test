import os
from selenium import webdriver

# Create an instance of Firefox WebDriver
#gecko = "/usr/local/bin"
#os.environ["geckodriver.firefox.driver"] = gecko
browser = webdriver.Firefox()

# KEY POINT: The driver.get method will navigate to a page given by the URL
browser.get('https://www.linkedin.com')

# Check if the title of the page is proper
if(browser.title=="Qxf2 Services: Selenium training main"):
    print ("Success: Qxf2 Tutorial page launched successfully")
else:
    print ("Failed: Qxf2 Tutorial page Title is incorrect") 

# Quit the browser window
browser.quit() 